<?php




// for-each loop
// Associative Array te index gulo thake key value pair akare.
$array = ['number1' => 10, 'number2' => 20, 'number3' => 30];

foreach ($array as $key => $value) {
    echo "$key = $value <br>";
}



echo '<br>';
echo '<br>';


// Associative Array er key and value gulo ke access karar jonno for-each loop use kora hoy.
$student = ['name' => "Rahim", 'email' => "rahim@gmail.com", 'result' => "4.00"];

foreach ($student as $key => $value) {
    echo "$key = $value <br>";
}

// 3 types of Array in PHP
// (i) Index Array: Je array te ame shudhu value gulo declare kore thake and tar index gulo default vhabe show kora thake.
// (i) Associative Array;
// (i) Multidimentional Array;

// Amra array dui vhabe declare korte pare: (1) Array() function er maddhome; (2) []- third bracket er maddhome;

// Index Array: Je array te shudhu value-gulo declare korle, tar index-gulo default akare ashe, sheguloke index array bole. Er important ekta boishishtha holo- era string akare thake.

echo '<br>';
echo '<br>';

// INDEX Array

$array = array('Rahim', 'Karim', 'Razzak', 'Kashem', 'Kamal');

echo '<pre>';
    print_r($array);
echo '</pre>';


echo '<br>';
echo '<br>';

$array = array('Rahim', '5' => 'Karim', 'Razzak', 'Kashem', 'Kamal');

echo '<pre>';
    print_r($array);
echo '</pre>';

echo '<br>';
echo '<br>';

// Associative array te- key value pair thakbe. Mane tar prottekta key er sathe value assign kora lagbe.

// ASSOCIATIVE Array

$info = ['name' => 'Kalam', 'email' => 'kalam@gmail.com' ,'id' => '1810225'];

echo '<pre>';
    print_r($info);
echo '</pre>';

echo '<br>';
echo '<br>';

// Multi-dimensional array


$department = [
    'student'=>['name' => 'Razzak', 'email' => 'razzak@gmail.com'],
    'skills'=>[
        'HTML',
        'CSS',
        'JS',
        'PHP'
    ],
];

echo $department['skills'][0];

echo "<pre>";
    print_r($department['skills']) ;
echo "</pre>";

echo $department['student']['email'];

echo '<br>';
echo '<br>';

print_r($department['skills']);

?>