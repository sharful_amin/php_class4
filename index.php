<?php




// echo 1 . '<br>';
// echo 2 . '<br>';
// echo 3 . '<br>';
// echo 4 . '<br>';
// echo 5 . '<br>';
// echo 6 . '<br>';
// echo 7 . '<br>';
// echo 8 . '<br>';
// echo 9 . '<br>';
// echo 10 . '<br>';


// For-loop

$number = 10;

for ($i=1; $i <= $number ; $i++) { 
    echo "The numbers is: $i".'<br>';
}


$familyInfo = ['Shakib', 'Rahim', 'Karim', 'Razzak', 'Kashem'];

// 'count' is a helper function. Built in PHP te 1000 of function ache, jegulo ke bola hoy helper function.
$length = count($familyInfo);

for ($i=0; $i<$length ; $i++) { 
    echo $familyInfo[$i].'<br>';
}

echo '<br>';
echo '<br>';

// for ($i=0; $i <= 4; $i++) { 
//     echo "Family member is " .$familyInfo[$i].'<br>';
// }

// while loop

$a = 1;

while($a <= 10){
    echo $a.'<br>';
    $a++;
}

echo '<br>';
echo '<br>';

// nested while loop

$a = 0;

while($a <= 10){
    $a=$a+1;
    // $a++;

    if($a==5){
        continue;
    }

    echo $a.'<br>';
}

echo '<br>';
echo '<br>';

// do-while loop
// do while loop e condition check korbe- ekbar print hoye jawar por.

$a = 12;

do {
    echo "The number is:" .  $a;
} while ($a <= 10);







?>